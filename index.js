let http = require("http");

const port = 3000;
const server = http.createServer((request,response)=>{
	if(request.url == '/login'){
		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end('Welcome to the log in page');
	}
	
	else{
		response.writeHead(404,{'Content-Type': 'text/plain'});
		response.end('Im sorry the page your looking for cannot be found')
	}
});

server.listen(port);
console.log(`Server now acccessible at localhost: ${port}`);